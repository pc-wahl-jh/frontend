/**
* Generate a Private and Public keypair
* @param  {numBits} Integer - Any multiple of 1024. 2048 is recommended.
* @param  {userid} String - should be like: Alice Mayfield <amayfield@quantum.com>
* @param  {passphrase} String - password should be a 4-5 word sentence (20+ chars)
* @return {key} String - Encrypted ASCII armored keypair (contains both Private and Public keys)
*/
function keygen() {
    var openpgp = window.openpgp;
    var key = openpgp.generateKey({
        numBits: 4096,
        userIds: [{ name:'Jon Smith', email:'jon@example.com' }],
        passphrase: ""
    });
    return key;
}

/**
 * Sign a message using your private key.
 * @param  {pubkey} String - Your recipient's public key.
 * @param  {privkey} String - Your private key.
 * @param  {passphrase} String - Your ultra-strong password.
 * @param  {message} String - Your message from the recipient.
 * @return {signed} String - Signed message.
 */
function sign_message(pubkey, privkey, passphrase, message){
	var openpgp = window.openpgp;
	var priv = openpgp.key.readArmored(privkey);
	var pub = openpgp.key.readArmored(pubkey);
	var privKey = priv.keys[0];
	var success = priv.decrypt(passphrase);
	var signed = openpgp.signClearMessage(priv.keys, message);
    return signed;
}

/**
 * Sign a message using your private key.
 * @param  {pubkey} String - Your recipient's public key.
 * @param  {privkey} String - Your private key.
 * @param  {passphrase} String - Your ultra-strong password.
 * @param  {signed_message} String - Your signed message from the recipient.
 * @return {signed} Boolean - True (1) is a valid signed message.
 */
function verify_signature(pubkey, privkey, passphrase, signed_message) {
    var openpgp = window.openpgp;
    var privKeys = openpgp.key.readArmored(privkey);
    var publicKeys = openpgp.key.readArmored(pubkey);
    var privKey = privKeys.keys[0];
    var success = privKey.decrypt(passphrase);
    var message = openpgp.cleartext.readArmored(signed_message);
    var verified = openpgp.verifyClearSignedMessage(publicKeys.keys, message);
    if (verified.signatures[0].valid === true) {
        return '1';
    } else {
        return '0';
    }
}