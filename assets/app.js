const app = {
    api_host: "http://localhost:3000",
    q: a => app.api_host + "/api/" + a
};

function jsonFromEndpoit(url) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "/api/" + url, false); // false for synchronous request
    xmlHttp.send(null);
    var response = xmlHttp.responseText;
    return JSON.parse(response);
}


var key;
$(() => {
    var st = localStorage;

    key = JSON.parse(st.getItem('key'));


    if(!key) {
        keygen().then(key => {
            st.setItem('key', JSON.stringify(key))
            console.log(key);

            key = JSON.parse(st.getItem('key'));

            $("#pub-key").html(key.publicKeyArmored);
        });
        key = JSON.parse(st.getItem('key'));
    }
    $("#pub-key").html(key.publicKeyArmored);

    $.getJSON("/api/kandidaten", data => $.each(data, (a, b) => {
        $("#erststimme").append("<label for=" + a + ">" + b.vorname + " " + b.nachname + "</label><input type='number' name=" + a +"></li>");
    }));
    $.getJSON("/api/parteien", data => $.each(data, (a, b) => {
        $("#zweitstimme").append("<label for=" + a + ">" + b.name + "</label><input type='number' name=" + a +"></li>");
    }));
    const g = name => $("input[name=" + name + "]").val()
    $("button").click(() => {
        erststimmen = {}
        for(kandidat in jsonFromEndpoit("kandidaten")) {
            if(g(kandidat))
                erststimmen[kandidat] = g(kandidat)
        }

        zweitstimmen = {}
        for(kandidat in jsonFromEndpoit("parteien")) {
            if(g(kandidat))
                zweitstimmen[kandidat] = g(kandidat)
        }

        var payload = {
            "erststimmen": {
                "stimmen": erststimmen
            },
            "zweitstimmen": {
                "stimmen": zweitstimmen
            }
        };

        console.log(JSON.stringify(payload))

        var unsigend_payload = JSON.stringify(payload);
        window.openpgp.sign({data: unsigend_payload, privateKeys: openpgp.key.readArmored(key.privateKeyArmored).keys[0]}).then(signature => {
            payload["signature"] = signature.data;
            $.ajax({
                type: 'PUT', // Use POST with X-HTTP-Method-Override or a straight PUT if appropriate.
                headers: {
                    "Content-Type": "application/json"
                },
                url: "/api/wahlen/" + $("select").val() + "-wahl", // A valid URL
                data: JSON.stringify(payload) // Some data e.g. Valid JSON as a string
            });
        })
    });
});